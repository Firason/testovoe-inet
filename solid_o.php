<?php
class SomeObject {
    protected $name;

    public function __construct(string $name) { }

    public function getObjectName() { }
}


interface ObjectHandler {
    public function handle(SomeObject $object): string;
}

class Object1Handler implements ObjectHandler {
    public function handle(SomeObject $object): string {
        return 'handle_object_1';
    }
}

class Object2Handler implements ObjectHandler {
    public function handle(SomeObject $object): string {
        return 'handle_object_2';
    }
}

class SomeObjectsHandler {
    protected $handlers = [];

    public function __construct() {
        $this->handlers['object_1'] = new Object1Handler();
        $this->handlers['object_2'] = new Object2Handler();
    }

    public function handleObjects(array $objects): array {
        $handlers = [];
        foreach ($objects as $object) {
            $objectName = $object->getObjectName();
            if (isset($this->handlers[$objectName])) {
                $handler = $this->handlers[$objectName];
                $handlers[] = $handler->handle($object);
            }
        }

        return $handlers;
    }
}