<?php
$array = [
    ['id' => 1, 'date' => '12.01.2020', 'name' => 'test1'],
    ['id' => 2, 'date' => '02.05.2020', 'name' => 'test2'],
    ['id' => 4, 'date' => '08.03.2020', 'name' => 'test4'],
    ['id' => 1, 'date' => '22.01.2020', 'name' => 'test1'],
    ['id' => 2, 'date' => '11.11.2020', 'name' => 'test4'],
    ['id' => 3, 'date' => '06.06.2020', 'name' => 'test3'],
];
// 1 задание
$filter = array_reduce($array, function ($unique, $item) {
    $id = $item['id'];
    if (!isset($unique[$id])) {
        $unique[$id] = $item;
    }
    return $unique;
}, []);

$uniqueArray = array_values($filter);
// 2 задание
//Для даты можно использовать Carbon или 
//function is_Date($str){
 //   return is_numeric(strtotime($str));
//}
//и сравнивать 
$sortBy = 'id';
usort($uniqueArray, function ($a, $b) use ($sortBy) {
    return $a[$sortBy] - $b[$sortBy];
});

// 3 задание
$searchBy = 'id';
$search = 1;
$searchedArray = array_filter($uniqueArray, function ($element) use ($searchBy, $search) {
    return $element[$searchBy] == $search;
});

//4 задание
$reversedArray = array_reduce($uniqueArray, function ($result, $item) {
    $result[$item['name']] = $item['id'];
    return $result;
}, []);

//5 задание

//SELECT id, name
//FROM goods
//WHERE id IN (
//    SELECT goods_id
//    FROM goods_tags
//    WHERE tag_id IN (
//        SELECT id FROM tags
//    )
//);

// 6 задание
// Не знаю решение без подзапроса

//SELECT *
//FROM departments
//WHERE id IN (
//    SELECT department_id
//    FROM evaluations
//    WHERE gender = true AND value > 5
//);
