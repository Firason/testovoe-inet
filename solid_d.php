<?php
class XMLHTTPRequestService implements XMLHTTPRequestServiceInterface
{
    public function request(string $url, string $method, array $options = []) {
        // Реализация запроса
    }
}

//Удаляется. Если не обходимо использовать конкретно этот класс нужно передекларировать метод request
//class XMLHttpService extends XMLHTTPRequestService {}

interface XMLHTTPRequestServiceInterface {
    public function request(string $url, string $method, array $options = []);
}

class Http
{
    private $service;

    public function __construct(XMLHTTPRequestServiceInterface $xmlHttpService)
    {
    }

    public function get(string $url, array $options)
    {
        $this->service->request($url, 'GET', $options);
    }

    public function post(string $url)
    {
        $this->service->request($url, 'GET');
    }
}