<?php
interface KeyStorageTypeInerface {
    public function getSecretKey();
}

// Реализация в файле
class FileKeyStorageType implements KeyStorageTypeInerface {
    private $filePath = config('outputSevices.api.secret_key_path');

    public function getSecretKey() {
        return file_get_contents($this->filePath);
    }
}

// Реализация из конфига
class ConfigKeyType implements KeyStorageTypeInerface {
    public function getSecretKey() {
        return config('outputSevices.api.secret_key');
    }
}

// Реализация в БД
class DbKeyStorageType implements KeyStorageTypeInerface {
    public function getSecretKey() {
       App\Models\Config::query()->select(['value'])->where('type', 'secret_key')->firstOrFail()->value;
    }
}

// Реализация в Redis
class RedisKeyStorageType implements KeyStorageTypeInerface {
    public function getSecretKey() {
        return \Redis::get('secret_key');
    }
}

class Concept {
    private $client;
    private $keyStorageStrategy;

    public function __construct(KeyStorageTypeInerface $keyStorageStrategy) {
        $this->client = new \GuzzleHttp\Client();
        $this->keyStorageStrategy = $keyStorageStrategy;
    }
// Для определения из конфига
//    public function __construct() {
//        $this->client = new \GuzzleHttp\Client();
//        switch (config('app.key.type')) {
//            case "db":
//                $this->keyStorageStrategy = new DbKeyStorageStrategy;
//                break;
//            //...
//        }
//    }

    public function getUserData() {
        $params = [
            'auth' => ['user', 'pass'],
            'token' => $this->keyStorageStrategy->getSecretKey()
        ];

        $request = new \Request('GET', 'https://api.method', $params);
        $promise = $this->client->sendAsync($request)->then(function ($response) {
            $result = $response->getBody();
        });

        $promise->wait();
    }
}

$typeKey = new FileKeyStorageType();
$concept = new Concept($typeKey);
$concept->getUserData();
